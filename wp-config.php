<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f/DjnotykWSw0yTSON1sceYzJVg6UKzyV3o77AP621wiPVTEHcYkklDuor+aEoQ0xw4jE9kK2+WamUhwXtf1NA==');
define('SECURE_AUTH_KEY',  'FznCvTP8d7VSJwIFMDbv97JOPeD3F6l/p9TQw6HcHd6RZcHv2tv52LmEhfmLp6CSDYYKATLF8HgrkcbXzVNNiQ==');
define('LOGGED_IN_KEY',    'RpLCgCPal6lgS579rrM2PB4bxGPIhQULdjJqsQj1coH52Bm2ZYSwFYu38594dtJUwArPO5/Wsi99GpFhUOO1xA==');
define('NONCE_KEY',        '82fmzdrR4ow6mFnR1Hz+QvRmYcgJ5ReFYlCgWVoAlxtXtSbMhEkM7JSojOTdJpAN8qhXKDqB+zzd8UDB1lpHig==');
define('AUTH_SALT',        'tW+k3FUb7PNo/k/CplbcVdgYEimOt/k7yjuDO3USlnHKxWobOvZY8Tm8Wtc6wLIqjECq/1cQhXOsleHeb24Piw==');
define('SECURE_AUTH_SALT', 'pJPeOuNonvZ+tn7umXd5D17tET+wgfPCLySZLJNOErch72XMhBNs5RV5pOn5ZOdlaZVGuoEQQEJYzqKphj/Jfg==');
define('LOGGED_IN_SALT',   'afKP/+H2cWayqOOS3SguGQK1KXIF8ivL7XBO6IOTyc9ME9SMu4+gskVE3jnY5CYHz+uiCr4yY6WlgyRDHV1igg==');
define('NONCE_SALT',       'c+t9G97r4AuiSZze+mFsLlhBwMMb0etOSsCjfnRKcAoW7SZfGkruCnAexoebuwngVa7k3ETFw5UPMKpWEvD8mw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
