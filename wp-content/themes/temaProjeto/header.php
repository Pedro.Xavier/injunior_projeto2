<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name') ?> | <?php the_title() ?></title>
    <?php wp_head() ?>
</head>
<body <?php body_class(); ?>>

<?php
    $img_url = get_stylesheet_directory_uri() . '/images';
?>

<header class='container'>
    <a href = "/page-home/" class = "home">
            <img src="<?= $img_url ?>/logo.png" alt =""> 
        </a>  
        <nav>
            <div class ="busca">
                <form action='<?php bloginfo("url");?>' method='get'>
                    <input type='text' name='s' id='s' placeholder='Buscar' value = '<?php the_search_query();?>'>
                    <input type='text' name='post_type' value='product' class='hidden'>
                    <input type='submit' id='searchbutton' value='Buscar'>
                </form>
            </div>   
        </nav>
    
        
    <nav>
    <button>Faça um pedido</button>
            <img class = 'carrinho'  src="<?= $img_url ?>/carrinho.png" alt =""> 
        
            <a href = "<?= get_stylesheet_directory_uri() . '/minha-conta';?>/" class = "minha-conta">
                <img src="<?= $img_url ?>/user.png" alt =""> 
            </a>
    </nav>

</header>
