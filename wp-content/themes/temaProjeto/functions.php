
<?php 
function get_link_category_img(){
        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;

        $args = array(
               'taxonomy'     => $taxonomy,
               'orderby'      => $orderby,
               'show_count'   => $show_count,
               'pad_counts'   => $pad_counts,
               'hierarchical' => $hierarchical,
               'title_li'     => $title,
               'hide_empty'   => $empty
        );
        $categorias_lista = [];
        $categories = get_categories($args);


        if ($categories){

           foreach($categories as $category){

            if($category->name !='Sem categoria'){

                        $category_id = $category->term_id;
                        $img_id = get_term_meta($category_id, 'thumbnail_id', true);
                        $categorias_lista[] = [
                            'name' => $category->name,
                            'id'=> $category_id,
                            'link'=> get_term_link($category_id, 'product_cat'),
                            'img'=> wp_get_attachment_image_src($img_id, 'slide')[0]
                        ];
                };

            };
        };
        return $categorias_lista;
    };
?>